import todoApi from '../apis/todoApi';
import history from '../history';
import { 
    CREATE_TODO,
    DELETE_TODO,
    EDIT_TODO,
    FETCH_TODO,
    FETCH_TODOS
} from "./types";

export const createTodo = (formValues) => async (dispatch, getState) => {
    const response = await todoApi.post('/todo', formValues);

    dispatch({ type: CREATE_TODO, payload: response.data });

    history.push('/');
};

export const fetchTodos = () => async (dispatch) => {
    const response = await todoApi.get('/todo');

    dispatch({ type: FETCH_TODOS, payload: response.data });
};

export const fetchTodo = (id) => async (dispatch) => {
    const response = await todoApi.get(`/todo/${id}`);

    dispatch({ type: FETCH_TODO, payload: response.data });
};

export const editTodo = (id, formValues) => async (dispatch) => {
    const response = await todoApi.patch(`/todo/${id}`, formValues);

    dispatch({ type: EDIT_TODO, payload: response.data });

    history.push('/');
};

export const deleteTodo = (id) => async (dispatch) => {
    await todoApi.delete(`/todo/${id}`);

    dispatch({ type: DELETE_TODO, payload: id });

    history.push('/');
}
