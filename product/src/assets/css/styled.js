import styled from 'styled-components';

export const HeaderTop = styled.div`
    background-color: black;
    height: 50px;
    display: flex;
    justify-content: space-around;
    align-items: center;
`;

export const Button = styled.button`
    color: #fff !important;
    text-transform: uppercase;
    text-decoration: none;
    background: #60a3bc;
    padding: 20px;
    border-radius: 50px;
    display: inline-block;
    border: none;
    transition: all 0.4s ease 0s;
`;

export const ListContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(270px, 1fr));
    justify-items: center;
    grid-gap: 15px;
    margin: 20px 0;
`;

export const List = styled.div`
    margin: 10px 0;
    padding: 25px;
    border: solid;
`;

export const InputContainer = styled.div`
    padding: 5px 15px;
`;

export const ModalContainer = styled.div`
    border: solid; 
    margin: 40px auto; 
    width: 500px; 
    padding: 20px;
`;

export const nonStyleBtn = (color) => {
    if (color !== 'red') color = '#60a3bc';
    return {
        color: 'white',
        textTransform: 'uppercase',
        textDecoration: 'none',
        background: color,
        padding: '20px',
        borderRadius: '50px',
        display: 'inline-block',
        border: 'none',
        transition: 'all 0.4s ease 0s',
        fontSize: '10px'
    }
}