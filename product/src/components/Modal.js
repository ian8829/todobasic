import React from 'react';
import ReactDOM from 'react-dom';
import { ModalContainer } from '../assets/css/styled';

export default function Modal(props) {
    return ReactDOM.createPortal(
        <ModalContainer onClick={props.onDismiss}>
            <div onClick={(e) => e.stopPropagation()}>
                <div style={{ fontSize: '20px' }}>{props.title}</div>
                <div style={{ fontSize: '15px', marginTop: '10px' }}>{props.content}</div>
                <div style={{ fontSize: '15px', marginTop: '10px' }}>{props.actions}</div>
            </div>
        </ModalContainer>,
        document.querySelector('#modal')
    );
};