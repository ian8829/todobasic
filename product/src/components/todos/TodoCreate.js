import React from 'react';
import { useDispatch } from "react-redux";
import { createTodo } from '../../actions';
import ProductForm from './TodoForm';

export default function TodoCreate() {
    const dispatch = useDispatch();

    const onSubmit = (formValues) => {
        console.log(`formValues? ${JSON.formValues}`);
        dispatch(createTodo(formValues));
    };

    return (
        <div style={{ textAlign: 'center' }}>
            <h2 style={{ margin: '15px 0' }}>Create Todo</h2>
            <ProductForm onSubmit={onSubmit} />
        </div>
    );
}