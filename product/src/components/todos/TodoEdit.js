import _ from 'lodash';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchTodo, editTodo } from '../../actions';
import TodoForm from './TodoForm';

export default function TodoEdit(props) {
    const todo = useSelector((state) => state.todos[props.match.params.id]);
    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            dispatch(fetchTodo(props.match.params.id));
        })();
    }, [props.match.params.id, dispatch]);

    const onSubmit = (formValues) => {
        dispatch(editTodo(props.match.params.id, formValues));
    };

    if (!todo) {
        return <div>Loading...</div>
    }
    return (
        <div style={{ textAlign: 'center' }}>
            <h2 style={{ margin: '15px 0' }}>Edit a Todo</h2>
            <TodoForm 
                initialValues={_.pick(todo, 'title', 'description')}
                onSubmit={onSubmit} 
            />
        </div>
    );
    
}