import React, { useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import Modal from '../Modal';
import history from '../../history';
import { fetchTodo, deleteTodo } from '../../actions';
import { nonStyleBtn } from '../../assets/css/styled';

export default function TodoDelete(props) {
    const todo = useSelector((state) => state.todos[props.match.params.id]);
    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            dispatch(fetchTodo((props.match.params.id)))
        })();
    }, [props.match.params.id, dispatch]);

    const renderActions = () => {
        const { id } = props.match.params;

        return (
            <Fragment>
                <button 
                    style={nonStyleBtn('red')} 
                    onClick={() => dispatch(deleteTodo(id))}
                >
                    Delete
                </button>
                <Link style={nonStyleBtn()} to="/">Cancel</Link>
            </Fragment>
        );
    }

    const renderContent = () => {
        if (!todo) {
            return '해당 todo 항목을 삭제하겠습니까?';
        }
        return `다음의 todo 항목을 삭제하겠습니까? 
                타이틀: ${todo.title}?`;
    }
    
    return (
        <Modal 
            title="Todo 삭제"
            content={renderContent()}
            actions={renderActions()}
            onDismiss={() => history.push('/')}
        />
    );
    
}