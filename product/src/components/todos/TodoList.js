import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import { fetchTodos } from '../../actions';
import { ListContainer, List, nonStyleBtn } from '../../assets/css/styled';

export default function TodoList() {
    const todos = useSelector((state) => Object.values(state.todos));
    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            dispatch(fetchTodos());
        })();
    }, [dispatch]);

    const renderList = () => {
        return todos.map(todo => {
            const completeSpan = <span style={{ color: 'red' }}>is completed..</span>;
            const completeStyle = todo.is_complete ? { fontSize: '20px', textDecoration: 'line-through' } : { fontSize: '20px' };

            return (
                <List key={todo.due_date}>
                    <div style={completeStyle}>
                        Title: {todo.title}
                    </div>
                    <div style={{ fontSize: '15px', marginTop: '10px' }}>
                        Due Date: ~ {todo.is_complete ?  completeSpan : todo.due_date}
                    </div>
                    <div style={{ fontSize: '15px', marginTop: '10px' }}>
                        Description: {todo.is_complete ?  completeSpan : todo.description}
                    </div>
                    <div style={{ marginTop: '15px', textAlign: 'center' }}>
                        <Link style={nonStyleBtn()} to={`/todo/edit/${todo.id}`}>Edit</Link>
                        <Link style={nonStyleBtn('red')} to={`/todo/delete/${todo.id}`}>Delete</Link>
                    </div>
                </List>
            );
        });
    }
    
    return (
        <div>
            <ListContainer>{renderList()}</ListContainer>
        </div>
    )
}