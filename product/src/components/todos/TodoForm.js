import React, { useState } from 'react';
import { Button, InputContainer } from '../../assets/css/styled';

export default function TodoForm(props) {
    const [ inputs, setInputs ] = useState({ 
        title: '', 
        description: '', 
        due_date: '', 
        is_complete: false 
    });

    const handleSubmit = (event) => {
        if (event) {
            event.preventDefault();
            props.onSubmit(inputs);
        }
    }

    const handleInputChange = (event) => {
        event.persist();
        const { target }  = event;
        const value = target.type === "checkbox" ? target.checked : target.value;

        setInputs(inputs => (
            {
                ...inputs, 
                [target.name]: value
            }
        ));
    }

    return (
        <form onSubmit={handleSubmit}>
            <InputContainer>
                <label>Enter Title: </label>
                <input 
                    type="text" 
                    name="title" 
                    value={inputs.title} 
                    onChange={handleInputChange} 
                    required 
                />
            </InputContainer>
            <InputContainer>
                <label>Enter Description: </label> 
                <input 
                    type="text" 
                    name="description" 
                    value={inputs.description} 
                    onChange={handleInputChange} 
                    required 
                />
            </InputContainer>
            <InputContainer>
                <label>Enter Due Date: </label> 
                <input 
                    type="text" 
                    name="due_date" 
                    value={inputs.due_date} 
                    onChange={handleInputChange} 
                    required 
                />
            </InputContainer>
            <InputContainer>
                <label>Is Complete</label>
                <input 
                    type="checkbox" 
                    name="is_complete" 
                    value={inputs.is_complete}
                    onChange={handleInputChange}
                />
            </InputContainer>
            <Button type="submit">Submit</Button>
        </form>
    );
}