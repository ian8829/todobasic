import React from 'react';
import { Link } from 'react-router-dom';
import { HeaderTop } from '../assets/css/styled';

const Header = () => {
    const linkStyle = {
        color: 'white',
        textDecoration: 'none'
    };

    return (
        <HeaderTop>
            <Link to="/" style={linkStyle}>Todo List</Link>
            <Link to="/todo/new" style={linkStyle}>Create Todo</Link>
        </HeaderTop>        
    );
};

export default Header;