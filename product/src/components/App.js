import React from 'react';
import { Router, Route } from 'react-router-dom';
import TodoCreate from './todos/TodoCreate';
import TodoEdit from './todos/TodoEdit';
import TodoDelete from './todos/TodoDelete';
import TodoList from './todos/TodoList';
import Header from './Header';
import history from '../history';

const App = () => {
    return (
        <div>
            <Router history={history}>
                <div>
                    <Header />
                    <Route path="/" exact component={TodoList} />
                    <Route path="/todo/new"  component={TodoCreate} />
                    <Route path="/todo/edit/:id" exact component={TodoEdit} />
                    <Route path="/todo/delete/:id" exact component={TodoDelete} />
                </div>
            </Router>
        </div>
    );
};

export default App;