export default function solution(pickachoo, candy) {
    let xp = 0;
    if (pickachoo < 0 || candy < 12) return xp;

    for (let i = pickachoo; i > 0; i--) {
        if (candy < 12) break;
        candy -= 11;
        xp += 500;
    }
    return xp;
};