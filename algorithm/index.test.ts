import { describe, test } from  'mocha';
import { expect } from 'chai';
import solution from './index';

describe('Pocketmon GO', () => {
    test('Expected 500', () => {
        expect(solution(1, 12)).to.equal(500);
    });
    test('Expected 6500', () => {
        expect(solution(13, 144)).to.equal(6500);
    })
})