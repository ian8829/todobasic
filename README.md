### 실행방법  
 ## algorithm 
    해당 폴더(algorithm)로 간 후 yarn or npm install 후 
    npm test or yarn test 로 index.test.ts 파일을 실행.

 ## todo 
    해당 폴더(algorithm)로 간 후 yarn or npm install 후 
    npm start or yarn start 로 json-server와 client를 동시에 실행한다.
    concurrently pakcage 활용.

    create todo => Header의 Create Todo 버튼 클릭. 이후 해당 폼 작성
    edit todo => Main Page 혹은 Header의 Todo List 버튼 클릭, 
                 이후 Edit 버튼 클릭, 해당 폼 작성.
                 !!! is_complet 체크 시 완료 처리. !!!
    delete todo => Main Page 혹은 Header의 Todo List 버튼 클릭, 
                  이후 Delete 버튼 클릭.

### 의견
    개인적으로 다른 일정과 부득이하게 겹치게 되어 많은 시간 투자하지 못한 점 죄송합니다.